#!/bin/bash

set -eo pipefail
[[ "$TRACE" ]] && set -x

export SONAR_VERSION=8.4.1.35646

DistUpgrade () {
    export DEBIAN_FRONTEND=noninteractive
    # run update twice as it fails to update properly sometimes
    apt-get update -qq
    apt-get update -qq
    apt-get dist-upgrade -qq
}

InstallMiddleWare() {
    export DEBIAN_FRONTEND=noninteractive
    apt-get install -qq dirmngr ca-certificates wget apache2-utils git unzip openjdk-11-jre-headless fontconfig awscli jq
}

CreateSystemUser() {
  useradd -c "SonarQube Service User" -m -r -s /bin/bash -U sonarqube
}

GetSourceCode() {
  /usr/bin/su - sonarqube -c "wget -q -O sonarqube.zip https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-$SONAR_VERSION.zip && unzip -qq sonarqube.zip && rm sonarqube.zip"
}

InstallSourceCode() {
  mv /home/sonarqube/sonarqube-8.4.1.35646 /opt/sonarqube
}

InstallSysCtl() {
  cat << EOF > /etc/sysctl.d/99-sonarqube.conf
vm.max_map_count=524288
fs.file-max=131072
EOF
}

InstallSecurityLimits() {
  cat << EOF > /etc/security/limits.d/99-sonarqube.conf
sonarqube     -      nofile       65536
sonarqube     -      nproc        65536
EOF
}

SetupSystemd() {
  cat << EOF > /etc/systemd/system/sonarqube.service
[Unit]
Description=SonarQube service
After=syslog.target network.target
Wants=syslog.target network.target

[Service]
Type=forking
User=sonarqube
Group=sonarqube
ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop
ExecReload=/opt/sonarqube/bin/linux-x86-64/sonar.sh restart
PIDFile=/opt/sonarqube/bin/linux-x86-64/./SonarQube.pid

TimeoutStartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
EOF
}

EnableService() {
  systemctl enable sonarqube.service
}

main() {
    DistUpgrade
    InstallMiddleWare
    CreateSystemUser
    GetSourceCode
    InstallSourceCode
    InstallSysCtl
    InstallSecurityLimits
    SetupSystemd
    EnableService
}

main $@
